﻿
# Nomenclature

* Dev IDE: the IDE you use to develop the plugin, with this IDE you run the test
  IDE
* Test IDE: the IDE you use to run/test the plugin

# Logging

See [the doc](https://plugins.jetbrains.com/docs/intellij/ide-infrastructure.html#logging)

```java
import com.intellij.openapi.diagnostic.Logger;

// Create logger with category "ConfLexer"
private static final Logger log = Logger.getInstance("ConfLexer");

// Log message with debug severity
log.debug(String.format("Enter state %s", stateToStr(state)));
```

In order to see the log in the dev IDE, open the run config (runide) -> logs, 
add an entry for the log files in the sandbox folder e.g.
`/home/andrea/git/antconf/build/idea-sandbox/system/log/*.log`

In the test IDE you have to enter the category (here "ConfLexer") sa debug log,
help -> Diagnostic Tools -> Debug Log Settings, add `ConfLexer`.
This allows you to see DEBUG level log messages in the dev IDE in the run tool
under the `log` tab

## Example: logging in JFlex lexer

```java
// Our logger
private static final Logger log = Logger.getInstance("ConfLexer");

// Wrapping begin() to automatically issue a debug log message
public void begin(int state) {
    // Log only if state changes
    if (yystate() != state) {
    log.debug(String.format(
    "Enter state %s after matching '%s'",
    stateToStr(state), yytext()));
    }
    yybegin(state);
}

private String stateToStr(int state) {
    switch (state) {
        case YYINITIAL:
            return "YYINITIAL";
        case VAR_EXPANSION:
            return "VAR_EXPANSION";
        case WAITING_VALUE:
            return "WAITING_VALUE";
        case WAITING_TEXT:
            return "WAITING_TEXT";
    }
    return "<UNKNOWN>";
}
```

# Parser
## The `pin` attribute

See [the Grammar-Kit doc](https://github.com/JetBrains/Grammar-Kit#attributes-for-error-recovery-and-reporting)

The `pin=x` attributes tells the parser to create a PSI element after 
the x-th parsed element, by default, all child elements must be parsed 
correctly for a PSI element to be created.

For example in:
```bnf
var_assignemnt ::= VARSET VARNAME EQ VARVALUE TILDE
```
VARSET, VARNAME, EQ VARVALUE and TILDE must all be present in order to
create a var_assignmeent element.

This would give:<br>
![no_var_assigment][no_pin_does_not_create_psi_element]<br>
Note that no PSI element has been created for var_assigment

If we set `pin=1` however, the PSI element is created as soon as the first
element is successfully parsed
```bnf
var_assignemnt ::= VARSET VARNAME EQ VARVALUE TILDE {pin=1}
```
would give:<br>
![with_var_assignment][with_pin_a_psi_element_is_created]<br>
Note that a PSI element for var_assigment has been created even tough
there are errors when parsing its children

For an element to be created, all parent elements in the parsing must also be
created, that means that it might be necessary to set a `pin` attribute to
parent elements in order to make it work in a child.

[no_pin_does_not_create_psi_element]: img_2.png
[with_pin_a_psi_element_is_created]: img_3.png

## The `recoverWhile` attribute

See [here](https://github.com/JetBrains/Grammar-Kit#attributes-for-error-recovery-and-reporting)
and [here](https://github.com/JetBrains/Grammar-Kit/blob/master/HOWTO.md#22-using-recoverwhile-attribute)

Note: Here we assume that the pin attribute is used

The `recoverWhile` attribute allows the parser to resume its normal operation
after a parse error, this avoids 'ruining' the parsing of the rest of the
file because of an error earlier.

Without `recoverWhile`, the parsing is broken once an error is encountered:<br>
![img.png][no_further_psi_elements_after_error]<br>
Note that no PSI element for the second var_assignment is created,
the parser just output the elements it receives from the lexer

If we set the `recoverWhile` attribute, we can tell the parser to consume tokens as
long as it matches the given value.
```bnf
var_assignemnt ::= (VARSET | VARSETD) VARNAME EQ VARVALUE TILDE
{
    pin=1
    recoverWhile=var_assignment_recover
}

private var_assignment_recover ::= !(VARSET | VARSETD)
```
This tells the parser that after an error, it should resume its operation once
it parses `VARSET` or `VARSETD`
Note that the recover element is private.<br>
![img_1.png][psi_for_next_var_assignement_created]<br>
Note that the PSI element for the second variable assignement has been created

[no_further_psi_elements_after_error]: img.png
[psi_for_next_var_assignement_created]: img_1.png

# PSI

## References

In this snippet
`Script.Print Version %{TheVersion}`
"TheVersion" (or "%{Theversion}"?) is the reference

In this snippet
`Var.Set MyVar = the value`
MyVar is the resolved element, or declaration , or the element that introduces
"MyVar"

### How it works
The `PSIElement` that introduces a reference must implement `getReference()`
or `getReferences()`. these methods return a `PSIReference` or an array of
`PSIRefernce`. The `PSIReference` must implement `resolve()`, which returns
the element to which the reference points, or null. The resolved element should
implement the `PSINamedElement` interface.

The reference element should implement `PsiNameIdentifierOwner` instead
of just `PsiNamedElement`, this allows to get the contained element that
gives the reference the name.

Let's say we have the following declaration (which is the reference for a
usage of `%{MyVar}`)

```
Var.Set MyVar=value~
```

The reference here is the variable assignment, the whole thing, however, the
element which gives the name is just `MyVar`. By implementing
`PsiNamedIdentifierOwner` in the variable assignemnt element, we can tell
IntelliJ that. One effect of this is that if we do "Go to declaration",
the cursor is positioned at the beginning of `MyVar`.

# Unittests

Tests are in the `src/tests` folder

    src/tests/java/youpackage/<your source files>
    src/tests/resoruces/testData/<test data>

## Parser

See [here](https://plugins.jetbrains.com/docs/intellij/testing-plugins.html)
and [here](https://plugins.jetbrains.com/docs/intellij/writing-tests-for-plugins.html)

There's a lot of magic for testing lexing and parsing. Create a class named
`XXXTest`, then create a test file to be parsed `XXXTestData.<your_file_ext>`,
override `getTestDataPath()` point to the test data folder.
The test also needs a file containing the expected PSI tree in text form.
When the test is run it will fail because the PSI tree file does not exist, it
is automatically created. *Why does the test fail if the file automatically
created?.* The second time the test is run it will pass. This obviously works
if you are confident that the parser is working correctly.

The PSI tree file can also be created by hand:
 - Run the `runIde` Gradle task, tools -> View PSI Structure...
 - Create the content of the file to parse -> Build PSI Tree -> Copi PSI
 - Create the file `XXXTestData.txt` in the test data folder

**It has been necessary to override the `loadFile()` method in order not to
trim the content of the file to parse (resulted in difference with white space
elements)**
