package org.andreapeter.antconf;

import com.intellij.psi.PsiReference;
import com.intellij.psi.ResolveResult;
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase;
import com.intellij.usageView.UsageInfo;

import java.util.Collection;
import org.andreapeter.antconf.psi.ConfVarAssignment;


public class SimpleCodeInsightTest extends LightJavaCodeInsightFixtureTestCase {

    public String getTestDataPath() {
        return "src/test/resources/testData";
    }

    public void testFindUsages() {
        Collection<UsageInfo> usages = myFixture.testFindUsages(
                "testFindUsages.conf", "simpleCodeInsightBase.conf");
        assertEquals(2, usages.size());
    }

    public void testReferences() {

        String expectedValue = "TheValue";
        PsiReference referenceAtCaret = myFixture.getReferenceAtCaretPositionWithAssertion(
                        "testReferences.conf", "simpleCodeInsightBase.conf");

        final ConfVarNameExpReference varNameExpReference =
                assertInstanceOf(referenceAtCaret, ConfVarNameExpReference.class);

        final ResolveResult[] resolveResults = varNameExpReference.multiResolve(false);
        for (ResolveResult result : resolveResults) {
            assertEquals(expectedValue, ((ConfVarAssignment) result.getElement()).getVarValue().getText());
        }
    }
}
