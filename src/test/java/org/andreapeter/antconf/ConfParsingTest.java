package org.andreapeter.antconf;

import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.CharsetToolkit;
import com.intellij.testFramework.ParsingTestCase;
import com.intellij.testFramework.TestDataFile;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class ConfParsingTest extends ParsingTestCase {

    public ConfParsingTest() {
        super("", "conf", new ConfParserDefinition());
    }

    public void testCommand() {
        doTest(true);
    }

    public void testNestedSimpleVarExpansion() {
        doTest(true);
    }

    public void testVarExpansion() {
        doTest(true);
    }

    public void testRpnExpansion() {
        doTest(true);
    }

    public void testSimpleIfElse() {
        doTest(true);
    }

    public void testSimpleCommand() {
        doTest(true);
    }

    public void testCommandExpansion() {
        doTest(true);
    }

    public void testVarAssignements() {
        doTest(true);
    }

    public void testSwitchCaseExpansion() {
        doTest(true);
    }

    /**
     * @return path to test data file directory relative to root of this module.
     */
    @Override
    protected String getTestDataPath() {
        return "src/test/resources/testData";
    }

    @Override
    protected boolean skipSpaces() {
        return false;
    }

    @Override
    protected boolean includeRanges() {
        return true;
    }

    /**
     * Loads the file .conf file to parse,
     * we have to override this in order not to trim the file content
     * @param name Name of the file
     * @return The content of the file
     */
    @Override
    protected String loadFile(@NotNull @TestDataFile String name) throws IOException
    {
        return FileUtil.loadFile(
                new File(myFullDataPath, name),
                CharsetToolkit.UTF8, true);
    }
}