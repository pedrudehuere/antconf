package org.andreapeter.antconf.lexer;

import com.intellij.psi.tree.IElementType;
import com.intellij.testFramework.UsefulTestCase;
import org.andreapeter.antconf.lexer.ConfLexer;
import org.andreapeter.antconf.lexer.expansion.Expansion;
import org.andreapeter.antconf.lexer.expansion.RPNExpansion;
import org.andreapeter.antconf.lexer.expansion.SwitchCaseExpansion;
import org.andreapeter.antconf.lexer.expansion.VariableExpansion;
import org.andreapeter.antconf.psi.ConfTypes;

import java.io.IOException;

/*
 * The lexer must be generated before running this test
 */

/**
 * We mangle some methods in order to ease testing, not very elegant =(
 */
class MyLexer extends ConfLexer
{
    public void yypushback(int number) { }
}

public class ConfLexerTest extends UsefulTestCase
{
    private MyLexer lexer;

    protected void setUp() throws Exception{
        super.setUp();
        lexer = new MyLexer();
    }

    public void testGetNextExpansion() {
        checkExpansion(null, "");
        checkExpansion(null, "some text");
        checkExpansion(null, "% {");
        checkExpansion(null, "% {?");
        checkExpansion(null, "% {#");

        checkExpansion(VariableExpansion.class, "abc %{ ?");
        checkExpansion(VariableExpansion.class, "abc%{myVariable}");
        checkExpansion(VariableExpansion.class, "abc %{myVa%{#ri%{?able}");
        checkExpansion(VariableExpansion.class, "abc %{myVa%{?ri%{#able}");

        checkExpansion(SwitchCaseExpansion.class, "abc %{?myVariable}");
        checkExpansion(SwitchCaseExpansion.class, "abc %{?myVa%{#ri%{able}");
        checkExpansion(SwitchCaseExpansion.class, "abc %{?myVa%{ri%{#able}");

        checkExpansion(RPNExpansion.class, "abc %{#myVariable}");
        checkExpansion(RPNExpansion.class, "abc %{#myVar%{ia%{?ble}");
        checkExpansion(RPNExpansion.class, "abc %{#myVar%{?ia%{ble}");
    }

    public void testEnterExitState() {
        ConfLexer lexer = new ConfLexer();
        assertEquals(0, lexer.getStateLevel());
        assertEquals(ConfLexer.YYINITIAL, lexer.getState());

        // Enter two states
        lexer.enterState(ConfLexer.RPN_EXPANSION);
        assertEquals(ConfLexer.RPN_EXPANSION, lexer.getState());
        lexer.enterState(ConfLexer.SWITCH_CASE_CASE);
        assertEquals(ConfLexer.SWITCH_CASE_CASE, lexer.getState());
        assertEquals(2, lexer.getStateLevel());

        // Exit one state
        try {
            // TODO Not sure why `assertNoException()` would throw IOException
            assertNoException(java.io.IOException.class, () ->
                    lexer.exitState(ConfLexer.SWITCH_CASE_CASE));
        } catch (IOException e) {
            fail();
        }
        assertEquals(ConfLexer.RPN_EXPANSION, lexer.getState());
        assertEquals(1, lexer.getStateLevel());

        // Exit wrong state, we expect an error
        assertThrows(
                Throwable.class,  // Error logs throw a Throwable during tests
                "Wrong exit state, requested: VAR_EXP_DEF_VAL, actual: RPN_EXPANSION",
                () -> lexer.exitState(ConfLexer.VAR_EXP_DEF_VAL)
        );
    }

    // TODO not sure this is a god test, we'll see...
    public void testParseExpansionContent() {
        lexer.setState(ConfLexer.VAR_EXPANSION);
        checkParsedElement(ConfTypes.VARNAME, "some content");
        assertEquals(ConfLexer.VAR_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.SOME_TEXT, "s%{ome content");
        assertEquals(ConfLexer.VAR_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.VAR_EXP_START, "%{ content");
        assertEquals(ConfLexer.VAR_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.SWITCH_CASE_START, "%{? content");
        assertEquals(ConfLexer.SWITCH_CASE_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.RPN_EXP_START, "%{# content");
        assertEquals(ConfLexer.RPN_EXPANSION, lexer.getState());

        lexer.setState(ConfLexer.SWITCH_CASE_CASE);
        checkParsedElement(ConfTypes.SOME_TEXT, "some content");
        assertEquals(ConfLexer.SWITCH_CASE_CASE, lexer.getState());
        checkParsedElement(ConfTypes.SOME_TEXT, "s%{ome content");
        assertEquals(ConfLexer.SWITCH_CASE_CASE, lexer.getState());
        checkParsedElement(ConfTypes.VAR_EXP_START, "%{ content");
        assertEquals(ConfLexer.VAR_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.SWITCH_CASE_START, "%{? content");
        assertEquals(ConfLexer.SWITCH_CASE_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.RPN_EXP_START, "%{# content");
        assertEquals(ConfLexer.RPN_EXPANSION, lexer.getState());

        lexer.setState(ConfLexer.RPN_EXPANSION);
        checkParsedElement(ConfTypes.SOME_TEXT, "some content");
        assertEquals(ConfLexer.RPN_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.SOME_TEXT, "s%{ome content");
        assertEquals(ConfLexer.RPN_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.VAR_EXP_START, "%{ content");
        assertEquals(ConfLexer.VAR_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.SWITCH_CASE_START, "%{? content");
        assertEquals(ConfLexer.SWITCH_CASE_EXPANSION, lexer.getState());
        checkParsedElement(ConfTypes.RPN_EXP_START, "%{# content");
        assertEquals(ConfLexer.RPN_EXPANSION, lexer.getState());
    }

    private void checkParsedElement(IElementType expected, String content) {
        assertInstanceOf(lexer.parseExpansionContent(content), expected.getClass());
    }

    private <T extends Expansion> void checkExpansion(Class<T> expected, String content) {
        if (expected == null) {
            assertNull(lexer.getNextExpansion(content));
        } else {
            assertInstanceOf(new ConfLexer().getNextExpansion(content), expected);
        }
    }
}
