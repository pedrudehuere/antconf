package org.andreapeter.antconf;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import java.util.HashMap;
import java.util.Map;
import org.andreapeter.antconf.psi.ConfFile;
import org.andreapeter.antconf.psi.ConfPsiCache;
import org.andreapeter.antconf.psi.ConfVarAssignment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.andreapeter.antconf.psi.ConfVarExpansion;


public class ConfUtil
{
    private static final boolean USE_CACHE = true;

    private static final Logger log = Logger.getInstance("ConfUtil");


    /**
     * Search the project for all variable assignments in project,
     * if cache is enabled result is cached
     * @param project The project to search
     * @return All variable assignments in the given project
     */
    public static List<ConfVarAssignment> findAllConfVarAssignments(Project project) {
        if (USE_CACHE) {
            return project.getService(ConfPsiCache.class).findAllConfVarAssignments(
                    () -> _findAllConfVarAssignments(project));
        } else {
            return _findAllConfVarAssignments(project);
        }
    }

    /**
     * Search the project for variable assignments of the given variable,
     * if cache is enabled the result is cached
     * @param project The project to search
     * @param varName The variable for which assignments are searched
     * @return Variable assignments of the given variable in the given project
     */
    public static List<ConfVarAssignment> findConfVarAssignmentsByVarName(Project project, String varName) {
        if (USE_CACHE) {
            return project.getService(ConfPsiCache.class).
                    findConfVarAssignmentsByName(varName, () -> findVarAssignmentsByName(project));
        } else {
            return findVarAssignmentsByName(project).getOrDefault(varName, Collections.emptyList());
        }
    }


    private static List<ConfVarAssignment> _findAllConfVarAssignments(Project project) {
        log.debug("_findAllConfVarAssignments");
        List<ConfVarAssignment> result = new ArrayList<>();
        Collection<VirtualFile> virtualFiles =
                FileTypeIndex.getFiles(ConfFileType.INSTANCE, GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile: virtualFiles) {
            ConfFile confFile = (ConfFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (confFile != null) {
                ConfVarAssignment[] assignemnts = PsiTreeUtil.getChildrenOfType(confFile, ConfVarAssignment.class);
                if (assignemnts != null) {
                    Collections.addAll(result, assignemnts);
                }
            }
        }
        return result;
    }

    private static Map<String, List<ConfVarAssignment>> findVarAssignmentsByName(Project project) {
        log.debug("findVarAssignmentsByName");
        List<ConfVarAssignment> allAssignments = findAllConfVarAssignments(project);
        HashMap<String, List<ConfVarAssignment>> assignmentsByName = new HashMap<>();
        for (ConfVarAssignment varAssignemnt : allAssignments) {
            String varName = varAssignemnt.getVariableName();
            if (!assignmentsByName.containsKey(varName)) {
                assignmentsByName.put(varName, new ArrayList<>());
            }
            assignmentsByName.get(varName).add(varAssignemnt);
        }
        return assignmentsByName;
    }

    public static List<ConfVarExpansion> findConfVarExpansions(Project project, String varName) {
        List<ConfVarExpansion> varExpansions = new ArrayList<>();
        Collection<VirtualFile> virtualFiles =
                FileTypeIndex.getFiles(ConfFileType.INSTANCE, GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile: virtualFiles) {
            ConfFile confFile = (ConfFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (confFile != null) {
                Collection<ConfVarExpansion> expansions =
                        PsiTreeUtil.findChildrenOfType(confFile, ConfVarExpansion.class);
                for (ConfVarExpansion expansion: expansions) {
                    if (varName.equals(expansion.getVariableName())) {
                        varExpansions.add(expansion);
                    }
                }
            }
        }
        return varExpansions;
    }

    public static List<ConfVarExpansion> findConfVarExpansions(Project project) {
        List<ConfVarExpansion> varExpansions = new ArrayList<>();
        Collection<VirtualFile> virtualFiles =
                FileTypeIndex.getFiles(ConfFileType.INSTANCE, GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile: virtualFiles) {
            ConfFile confFile = (ConfFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (confFile != null) {
                Collection<ConfVarExpansion> expansions =
                        PsiTreeUtil.findChildrenOfType(confFile, ConfVarExpansion.class);
                varExpansions.addAll(expansions);
            }
        }
        return varExpansions;
    }
}
