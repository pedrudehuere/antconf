// Copyright 2000-2020 JetBrains s.r.o. and other contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
package org.andreapeter.antconf.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import org.andreapeter.antconf.psi.ConfTypes;
import com.intellij.psi.TokenType;

%%

%public
%abstract
%class AbstractConfLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

%{
    // Abstract methods
    abstract void enterState(int newState);
    abstract void exitState(int state) throws java.io.IOException;

    abstract IElementType parseExpansionContent(String content);
    abstract IElementType logElement(IElementType element);

    public int getState() {
        return yystate();
    }

    void setState(int newState) {
        yybegin(newState);
    }
%}

// Constructor code
%init{
%init}


CRLF=\R
WHITE_SPACE=[\ \n\t\f]

IF = "if"
ELSIF = "elsif"
ELSE = "else"
ENDIF = "endif"

VARSET = "Var.Set"
VARSETD = "Var.SetD"
VARSETE = "Var.SetE"
VARSETF = "Var.SetF"
VARAPP = "Var.Append"

SCRIPT_PRINT = "Script.Print"
SCRIPT_REM = "Script.Rem"
COMMAND = [A-Za-z][A-Za-z0-9.]*

// TODO does this regex match Var.Set and Var.SetD or does the lexer stop at first match?
VARNAME = [A-Za-z/*][A-Za-z0-9/*.@_]*
//DEF_VALUE = [A-Za-z0-9/*.@_]*
DEF_VALUE = [^}]*

EQ = "="
TILDE = "~"

%state COMMAND_EXPANSION
%state RPN_EXPANSION
%state SWITCH_CASE_EXPANSION
%state SWITCH_CASE_CASE
%state VAR_EXPANSION
%state VAR_EXP_DEF_VAL
%state WAITING_VALUE
%state WAITING_VARNAME
%state WAITING_TEXT

%%

/* From the doc:
 * As the scanner reads its input, it keeps track of all regular expressions and
 * activates the action of the expression that has the LONGEST MATCH.
 * If two regular expressions both have the longest match for a certain input,
 * the scanner chooses the action of the expression that appears FIRST in the specification.
 */

({CRLF}|{WHITE_SPACE})+   { return logElement(TokenType.WHITE_SPACE); }

<YYINITIAL> {
    {TILDE}               { return logElement(ConfTypes.TILDE); }
    {IF}                  { return logElement(ConfTypes.IF); }
    {ELSIF}               { return logElement(ConfTypes.ELSIF); }
    {ELSE}                { return logElement(ConfTypes.ELSE); }
    {ENDIF}               { return logElement(ConfTypes.ENDIF); }
    {VARSET}              { enterState(WAITING_VARNAME); return logElement(ConfTypes.VARSET); }
    {VARSETD}             { enterState(WAITING_VARNAME); return logElement(ConfTypes.VARSETD); }
    {VARSETE}             { enterState(WAITING_VARNAME); return logElement(ConfTypes.VARSETE); }
    {VARSETF}             { enterState(WAITING_VARNAME); return logElement(ConfTypes.VARSETF); }
    {VARAPP}              { enterState(WAITING_VARNAME); return logElement(ConfTypes.VARAPP); }
    {SCRIPT_PRINT}        { enterState(WAITING_TEXT); return logElement(ConfTypes.SCRIPT_PRINT); }
    {SCRIPT_REM}          { enterState(WAITING_TEXT); return logElement(ConfTypes.SCRIPT_REM); }
    // Generic COMMAND must be last in order to be overridden by specific commands
    {COMMAND}             { enterState(WAITING_TEXT); return logElement(ConfTypes.COMMAND); }
}

<WAITING_VARNAME> {
    {VARNAME}             { return logElement(ConfTypes.VARNAME); }
    {EQ}                  { enterState(WAITING_VALUE); return logElement(ConfTypes.EQ); }
}

<WAITING_VALUE> [^~]+     { return logElement(ConfTypes.VARVALUE); }
<WAITING_VALUE> {TILDE}   { exitState(WAITING_VALUE); exitState(WAITING_VARNAME); return logElement(ConfTypes.TILDE); }

<WAITING_TEXT>  {
    // This is not very elegant, it creates many small SOME_TEXT elements
    [^%~]+ | %[^{~]       { return logElement(ConfTypes.SOME_TEXT); }
    "~"                   { exitState(WAITING_TEXT); return logElement(ConfTypes.TILDE); }
    %\{                   { enterState(VAR_EXPANSION); return logElement(ConfTypes.VAR_EXP_START); }
    %\{\?                 { enterState(SWITCH_CASE_EXPANSION); return logElement(ConfTypes.SWITCH_CASE_START); }
    %\{#                  { enterState(RPN_EXPANSION); return logElement(ConfTypes.RPN_EXP_START); }
    %\{\!                 { enterState(COMMAND_EXPANSION); return logElement(ConfTypes.COMMAND_EXP_START); }
}

// Simple variable expansion
<VAR_EXPANSION> {
    [^}:]+                { return logElement(parseExpansionContent(yytext().toString())); }
    {VARNAME}             { return logElement(ConfTypes.VARNAME); }
    "}"                   { exitState(VAR_EXPANSION); return logElement(ConfTypes.VAR_EXP_END); }
    ":"                   { enterState(VAR_EXP_DEF_VAL); return logElement(ConfTypes.DEF_VALUE_SEP); }
}

<VAR_EXP_DEF_VAL> {
    {DEF_VALUE}           { return logElement(ConfTypes.SOME_TEXT); }
    // Here we also exit VAR_EXPANSION state
    "}"                   {
        exitState(VAR_EXP_DEF_VAL);
        exitState(VAR_EXPANSION);
        return logElement(ConfTypes.VAR_EXP_END);
    }
}

// Switch case expansion
<SWITCH_CASE_EXPANSION> {
    [^}:|]+               { return logElement(parseExpansionContent(yytext().toString())); }
//    %\{                   { enterState(VAR_EXPANSION); return logElement(ConfTypes.VAR_EXP_START); }
    "|"                   { enterState(SWITCH_CASE_CASE); return logElement(ConfTypes.CASE_SEP); }
}

<SWITCH_CASE_CASE> {
//    %\{                   { enterState(VAR_EXPANSION); return logElement(ConfTypes.VAR_EXP_START); }
    [^}:|]+               { return logElement(parseExpansionContent(yytext().toString())); }
//    [^%~|:}]+ | %[^{:|}~] { return logElement(ConfTypes.SOME_TEXT); }
    ":"                   { return logElement(ConfTypes.CASE_VALUE_SEP); }
    "|"                   { return logElement(ConfTypes.CASE_SEP); }
    // Here we also exit VAR_EXPANSION state
    "}"                   {
        exitState(SWITCH_CASE_CASE);
        exitState(SWITCH_CASE_EXPANSION);
        return logElement(ConfTypes.VAR_EXP_END);
    }
}

<RPN_EXPANSION> {
    [^}]+                 { return logElement(parseExpansionContent(yytext().toString())); }
//    [^%~}]+ | %[^{}~]     { return logElement(ConfTypes.SOME_TEXT); }
//    %\{                   { enterState(VAR_EXPANSION); return logElement(ConfTypes.VAR_EXP_START); }
    "}"                   { exitState(RPN_EXPANSION); return logElement(ConfTypes.VAR_EXP_END); }
}

<COMMAND_EXPANSION> {
    [^}]+                 { return logElement(ConfTypes.SOME_TEXT); }
    "}"                   { exitState(COMMAND_EXPANSION); return logElement(ConfTypes.VAR_EXP_END); }
}

// This  must be at the end
[^]                       { return logElement(TokenType.BAD_CHARACTER); }


/* TODO
 * 1. In "Var.Set Var.Set = value ~" the second Var.Set is not seen as a variable name
 *    but as Var.Set, this means we cannot have variables which are named 'Var.Set' or 'Var.SetD'
 */
