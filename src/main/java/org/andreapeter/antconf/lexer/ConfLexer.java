package org.andreapeter.antconf.lexer;

import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import org.andreapeter.antconf.lexer.expansion.*;
import org.andreapeter.antconf.psi.ConfTypes;

import com.intellij.openapi.diagnostic.Logger;
import org.jetbrains.annotations.Nullable;

import java.util.Stack;

/**
 * Lexer for the conf language, extends the abstract generated FlexLexer,
 * the flex lexer must be generated from Conf.flex before using this class
 */
public class ConfLexer extends AbstractConfLexer
{
    public ConfLexer() {
        super(null);
    }

    // Our logger
    private final Logger log = Logger.getInstance("ConfLexer");

    // Level of lexer status
    private int stateLevel = 0;

    // Lexer statuses are kept in a stack,
    // this allows us to track stack depth which is useful for debugging
    private final Stack<Integer> stateStack = new Stack<>();

    // Must be set when we enter any expansion state and cleared once we parse something
    private boolean justEnteredExpansion = false;

    public int getStateLevel() {
        return stateLevel;
    }

    /**
     * Pushes state to stack and enters it
     * @param newState State to enter
     */
    void enterState(int newState) {
        if (newState == VAR_EXPANSION
                || newState == SWITCH_CASE_EXPANSION
                || newState == RPN_EXPANSION) {
            justEnteredExpansion = true;
        }
        stateStack.push(yystate());
        stateLevel++;
        begin(newState);
    }

    /**
     * Exit the given state and enter the previous one,
     * the only reason for requiring the state to exit is for debugging, if it does not
     * match an exception is thrown.
     * The reason for which we raise IOException and not something more appropriate is that
     * the Lexer interface is defined in the SDK as we cannot change it
     * @param state State to exit
     * @throws java.io.IOException In case the given state does not match the current state
     */
    void exitState(int state) throws java.io.IOException {
        if (state != yystate()) {
            String message = String.format(
                    "Wrong exit state, requested: %s, actual: %s",
                    stateToStr(state),
                    stateToStr(yystate())
            );
            log.error(message);
            throw new java.io.IOException(message);
        }
        int prevState = stateStack.pop();
        stateLevel--;
        begin(prevState);
    }


    /**
     * Logs given state and enters it
     * @param state The state to begin
     */
    public void begin(int state) {
        log.debug(String.format(
                "%sS %s after matching '%s' at pos %d",
                "-".repeat(stateLevel),
                stateToStr(state),
                yytext(),
                getTokenStart()));
        yybegin(state);
    }

    /**
     * Logs element and returns it
     * @param element The element to log
     * @return The given element
     */
    IElementType logElement(IElementType element) {
        log.debug(String.format(
                "%sT %s \"%s\"",
                "-".repeat(stateLevel),
                element.toString(),
                yytext()));
        return element;
    }

    // TODO this needs to be updated by hand, not very... handy
    private String stateToStr(int state) {
        switch (state) {
            case YYINITIAL:
                return "YYINITIAL";
            case COMMAND_EXPANSION:
                return "COMMAND_EXPANSION";
            case RPN_EXPANSION:
                return "RPN_EXPANSION";
            case SWITCH_CASE_EXPANSION:
                return "SWITCH_CASE_EXPANSION";
            case SWITCH_CASE_CASE:
                return "SWITCH_CASE_CASE";
            case VAR_EXPANSION:
                return "VAR_EXPANSION";
            case VAR_EXP_DEF_VAL:
                return "VAR_EXP_DEF_VAL";
            case WAITING_VALUE:
                return "WAITING_VALUE";
            case WAITING_VARNAME:
                return "WAITING_VARNAME";
            case WAITING_TEXT:
                return "WAITING_TEXT";
        }
        return "<UNKNOWN>";
    }

    /**
     * Returns the next variable expansion in content
     */
    @Nullable
    Expansion getNextExpansion(String content) {
       final int ST_INIT = 0;   // Got nothing interesting
       final int ST_GOT_1 = 1;  // Got "%"
       final int ST_GOT_2 = 2;  // Got "%{"

        int state = ST_INIT;
        for (char ch : content.toCharArray()) {
            switch (state) {
                case ST_INIT:
                    if (ch == '%') {
                        state = ST_GOT_1;
                    }
                    break;
                case ST_GOT_1:
                    if (ch == '{') {
                        state = ST_GOT_2;
                    } else {
                        state = ST_INIT;
                    }
                    break;
                case ST_GOT_2:
                    if (ch == '?') {
                        return new SwitchCaseExpansion();
                    } else if (ch == '#') {
                        return new RPNExpansion();
                    } else {
                        return new VariableExpansion();
                    }
            }
        }
        if (state == ST_GOT_2) {
            return new VariableExpansion();
        }
        return null;
    }

    /**
     * @return The expansion from current state
     * Note: every state calling parseExpansionContent must be taken into account
     */
    @Nullable
    private Expansion getExpansionFromCurrentState() {
        switch(yystate()) {
            case VAR_EXPANSION:
                return new VariableExpansion();
            case SWITCH_CASE_EXPANSION:
            case SWITCH_CASE_CASE:
                return new SwitchCaseExpansion();
            case RPN_EXPANSION:
                return new RPNExpansion();
        }
        return null;
    }

    /**
     * Analyses the content of a variable expansion and returns the right element,
     * might enter a state if necessary, pushes back any remaining text if necessary
     * @param content The content of a variable expansion
     * @return The element to return for the parser
     */
    IElementType parseExpansionContent(String content) {

        // check if the content of the variable expansion contains another variable expansion
        int indexBegin = content.indexOf("%{");

        // Infer current expansion from lexer state
        Expansion currentExpansion = getExpansionFromCurrentState();
        if (currentExpansion == null) {
            log.error("No current expansion found");
            return TokenType.BAD_CHARACTER;
        }

        if (indexBegin > -1) {
            // the content of the variable expansion contains another expansion,
            // check which one
            Expansion nextExpansion = getNextExpansion(content);
            if (nextExpansion == null) {
                log.error("No next expansion found");
                return TokenType.BAD_CHARACTER;
            }

            if (indexBegin == 0) {
                // the next expansion is at the beginning, we eat the begin element ot the next
                // expansion and push back any text that follows
                yypushback(yylength() - nextExpansion.getStartString().length());
                // we enter VAR_EXPANSION state recursively
                enterState(nextExpansion.getEntryState());
                return nextExpansion.getStartElement();
            } else {
                // there is some text before the variable expansion, we eat the text before
                // the beginning of the next expansion and push back the rest
                yypushback(yylength() - indexBegin);
                justEnteredExpansion = false;
                return nextExpansion.getElementTypeForTextBeforeExpansion();
            }
        } else {
            // No expansion found
            if (justEnteredExpansion) {
                justEnteredExpansion = false;
                // We are at the very beginning in the epxansion, it's aVARNAME
                return currentExpansion.getElementTypeForOnlyText();
            } else {
                // We are somewhere else in the expansion, this is just some text
                return currentExpansion.getElementTypeForTextAfterExpansion();
            }
        }
    }
}
