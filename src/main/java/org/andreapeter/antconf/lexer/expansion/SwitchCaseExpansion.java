package org.andreapeter.antconf.lexer.expansion;

import com.intellij.psi.tree.IElementType;
import org.andreapeter.antconf.lexer.AbstractConfLexer;
import org.andreapeter.antconf.psi.ConfTypes;

/**
 * Switch case expansion, one method to achieve flow control
 * in the form: "%{?switch|case1:value1|case2:value2|default}"
 * where switch is normally a variable expansion, casex and valuex can be empty.
 * Will be replaces with valuex if switch has the value of casex or default if
 * no casex matches.
 * Can contain other expansions.
 */
public class SwitchCaseExpansion extends Expansion
{

    @Override
    public IElementType getStartElement()
    {
        return ConfTypes.SWITCH_CASE_START;
    }

    @Override
    public String getStartString()
    {
        return "%{?";
    }

    @Override
    public int getEntryState() {
        return AbstractConfLexer.SWITCH_CASE_EXPANSION;
    }
}
