package org.andreapeter.antconf.lexer.expansion;

import com.intellij.psi.tree.IElementType;
import org.andreapeter.antconf.psi.ConfTypes;

/**
 * Describes how an expansion has to be parsed
 */
public abstract class Expansion
{
    /**
     * @return The element that denotes the start of the expansion
     */
    public abstract IElementType getStartElement();

    /**
     * @return The string that denotes the start of the expansion
     */
    public abstract String getStartString();

    /**
     * @return The lexer state to enter when we enter the state
     */
    public abstract int getEntryState();

    /**
     * @return element type if the expansion does not contain other expansions
     */
    public IElementType getElementTypeForOnlyText() {
        return ConfTypes.SOME_TEXT;
    }

    /**
     * @return element type for any text preceding a nested expansion
     */
    public IElementType getElementTypeForTextBeforeExpansion() {
        return ConfTypes.SOME_TEXT;
    }

    /**
     * @return element type for eny text following a nested expansion
     */
    public IElementType getElementTypeForTextAfterExpansion() {
        return ConfTypes.SOME_TEXT;
    }
}
