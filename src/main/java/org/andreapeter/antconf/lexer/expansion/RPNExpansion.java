package org.andreapeter.antconf.lexer.expansion;

import com.intellij.psi.tree.IElementType;
import org.andreapeter.antconf.lexer.AbstractConfLexer;
import org.andreapeter.antconf.psi.ConfTypes;

/**
 * Reverse Polish notation
 * in the form: "%{# base height * 2 / }"
 * Will be replaced with the result
 * Can contain other expansions.
 */
public class RPNExpansion extends Expansion
{

    @Override
    public IElementType getStartElement()
    {
        return ConfTypes.RPN_EXP_START;
    }

    @Override
    public String getStartString()
    {
        return "%{#";
    }

    @Override
    public int getEntryState() {
        return AbstractConfLexer.RPN_EXPANSION;
    }
}
