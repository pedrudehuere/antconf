package org.andreapeter.antconf.lexer.expansion;

import com.intellij.psi.tree.IElementType;
import org.andreapeter.antconf.lexer.AbstractConfLexer;
import org.andreapeter.antconf.psi.ConfTypes;

/**
 * Variable expansion
 * in the form: "%{myVariable:default}"
 * :default is optional and cannot contain any expansion,
 * however, myVariable can contain other expansions.
 * Will be replaced by the variable set with the Var.Set command family.
 */
public class VariableExpansion extends Expansion
{

    @Override
    public IElementType getStartElement()
    {
        return ConfTypes.VAR_EXP_START;
    }

    @Override
    public String getStartString()
    {
        return "%{";
    }

    @Override
    public int getEntryState() {
        return AbstractConfLexer.VAR_EXPANSION;
    }

    @Override
    public IElementType getElementTypeForOnlyText()
    {
        return ConfTypes.VARNAME;
    }
}
