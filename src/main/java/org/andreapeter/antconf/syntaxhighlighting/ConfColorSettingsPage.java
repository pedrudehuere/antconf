package org.andreapeter.antconf.syntaxhighlighting;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import com.intellij.openapi.util.NlsContexts;
import org.andreapeter.antconf.Icons;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

public class ConfColorSettingsPage implements ColorSettingsPage
{

    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
        new AttributesDescriptor("Var.Set", ConfSyntaxHighlighter.VAR_SET),
        new AttributesDescriptor("Var.SetD|E|F", ConfSyntaxHighlighter.VAR_SETD),
        new AttributesDescriptor("Var.Append", ConfSyntaxHighlighter.VAR_APPEND),
        new AttributesDescriptor("Expansion open/close", ConfSyntaxHighlighter.EXPANSION_OPEN_CLOSE),
        new AttributesDescriptor("Command", ConfSyntaxHighlighter.COMMAND),
        new AttributesDescriptor("Flow xontrol", ConfSyntaxHighlighter.FLOW_CONTROL),
        new AttributesDescriptor("Tilde", ConfSyntaxHighlighter.TILDE),
    };

    @Override
    public @Nullable Icon getIcon()
    {
        return Icons.CONF_FILE;
    }

    @Override
    public @NotNull SyntaxHighlighter getHighlighter()
    {
        return new ConfSyntaxHighlighter();
    }

    @Override
    public @NonNls
    @NotNull String getDemoText()
    {
        return  "Script.Rem Check which board we are on~\n" +
                "if Script.Compare \"%{!BootSource.BoardName}\" = \"MPX-8349\" ~\n" +
                "    Var.Set ReservedUARTNb=1~\n" +
                "elsif Script.Compare \"%{!BootSource.BoardName}\" = \"QorIQ-T1024\" ~\n" +
                "    Script.Print IMU enabled: %{IMU.enabled}~\n" +
                "endif ~" +
                "\n" +
                "Var.Append Laser.xml=\n" +
                "    <SOPAS>\n" +
                "        <NAME>RearLaser</NAME>\n" +
                "        <IPRT>IpRt</IPRT>\n" +
                "        <ADDRESS>%{RearLaser.address}</ADDRESS>\n" +
                "        <MIRRORFREQ>%{?%{RearLaser.angleResolution}|quarter:25|50}</MIRRORFREQ>\n" +
                "        <SUPERVISOR>TRUE</SUPERVISOR>\n" +
                "    </SOPAS>~\n" +
                "Var.SetD SteeringOutN.left90=%{#16384 %{SteeringOutP.left90} -}~\n";
    }

    @Override
    public @Nullable Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap()
    {
        return null;
    }

    @Override
    public AttributesDescriptor @NotNull [] getAttributeDescriptors()
    {
        return DESCRIPTORS;
    }

    @Override
    public ColorDescriptor @NotNull [] getColorDescriptors()
    {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @Override
    public @NotNull @NlsContexts.ConfigurableName String getDisplayName()
    {
        return "ANT conf";
    }
}
