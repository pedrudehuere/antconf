package org.andreapeter.antconf.syntaxhighlighting;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import org.andreapeter.antconf.ConfLexerAdapter;
import org.andreapeter.antconf.psi.ConfTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;


public class ConfSyntaxHighlighter extends SyntaxHighlighterBase {

    public static final TextAttributesKey COMMAND =
            createTextAttributesKey("COMMAND", DefaultLanguageHighlighterColors.KEYWORD);

    public static final TextAttributesKey FLOW_CONTROL =
            createTextAttributesKey("FLOW_CONTROL", DefaultLanguageHighlighterColors.KEYWORD);

    public static final TextAttributesKey VAR_SET =
            createTextAttributesKey("ANTCONF_VAR_SET",  DefaultLanguageHighlighterColors.KEYWORD);

    public static final TextAttributesKey VAR_SETD =
            createTextAttributesKey("ANTCONF_VAR_SETD",  DefaultLanguageHighlighterColors.KEYWORD);

    public static final TextAttributesKey VAR_APPEND =
            createTextAttributesKey("ANTCONF_VAR_APPEND",  DefaultLanguageHighlighterColors.KEYWORD);

    public static final TextAttributesKey EXPANSION_OPEN_CLOSE =
            createTextAttributesKey("EXPANSION_OPEN_CLOSE", DefaultLanguageHighlighterColors.MARKUP_ENTITY);

    public static final TextAttributesKey TILDE =
            createTextAttributesKey("TILDE", DefaultLanguageHighlighterColors.KEYWORD);

    public static final TextAttributesKey TEXT =
            createTextAttributesKey("SIMPLE_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);

    public static final TextAttributesKey BAD_CHARACTER =
            createTextAttributesKey("SIMPLE_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);

    private static final TextAttributesKey[] COMMAND_KEYS = new TextAttributesKey[]{COMMAND};
    private static final TextAttributesKey[] FLOW_CONTROL_KEYS = new TextAttributesKey[]{FLOW_CONTROL};
    private static final TextAttributesKey[] VAR_SET_KEYS = new TextAttributesKey[]{VAR_SET};
    private static final TextAttributesKey[] VAR_SETD_KEYS = new TextAttributesKey[]{VAR_SETD};
    private static final TextAttributesKey[] VAR_APPEND_KEYS = new TextAttributesKey[]{VAR_APPEND};
    private static final TextAttributesKey[] EXP_OPEN_CLOSE = new TextAttributesKey[]{EXPANSION_OPEN_CLOSE};
    private static final TextAttributesKey[] TILDE_KEYS = new TextAttributesKey[]{TILDE};
    private static final TextAttributesKey[] TEXT_KEYS = new TextAttributesKey[]{TEXT};
    private static final TextAttributesKey[] BAD_CHARACTER_KEYS = new TextAttributesKey[]{BAD_CHARACTER};
    private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new ConfLexerAdapter();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType)
    {
        if (ElementGroups.COMMAND_ELEMENTS.contains(tokenType)) {
            return COMMAND_KEYS;
        } else if (ElementGroups.FLOW_CONTROL_ELEMENTS.contains(tokenType)) {
            return FLOW_CONTROL_KEYS;
        } else if (ElementGroups.VAR_SET_ELEMENTS.contains(tokenType)) {
            return VAR_SET_KEYS;
        } else if (ElementGroups.EXPANSION_OPEN_CLOSE.contains(tokenType)) {
            return EXP_OPEN_CLOSE;
        } else if (tokenType.equals(ConfTypes.VARSETD)) {
            return VAR_SETD_KEYS;
        } else if (tokenType.equals(ConfTypes.VARAPP)) {
            return VAR_APPEND_KEYS;
        } else if (tokenType.equals(ConfTypes.TILDE)) {
            return TILDE_KEYS;
        } else if (tokenType.equals(TokenType.BAD_CHARACTER)) {
            return BAD_CHARACTER_KEYS;
        } else {
            return EMPTY_KEYS;
        }
    }
}
