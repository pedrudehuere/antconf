package org.andreapeter.antconf.syntaxhighlighting;

import com.intellij.psi.tree.IElementType;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.andreapeter.antconf.psi.ConfTypes.*;

/**
 * Element groups for syntax highlighting
 */
public class ElementGroups
{
    public static final Set<IElementType> FLOW_CONTROL_ELEMENTS = new HashSet<>(
            Arrays.asList(IF, ELSIF, ELSE, ENDIF)
    );

    public static final Set<IElementType> COMMAND_ELEMENTS = new HashSet<>(
            Arrays.asList(COMMAND, SCRIPT_PRINT, SCRIPT_REM)
    );

    public static final Set<IElementType> VAR_SET_ELEMENTS = new HashSet<>(
            Arrays.asList(VARSET, VARSETE, VARSETF)
    );

    public static final Set<IElementType> EXPANSION_OPEN_CLOSE = new HashSet<>(
            Arrays.asList(
                    VAR_EXP_START,
                    SWITCH_CASE_START,
                    RPN_EXP_START,
                    COMMAND_EXP_START,
                    VAR_EXP_END)
    );
}
