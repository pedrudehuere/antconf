package org.andreapeter.antconf;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiElement;
import org.andreapeter.antconf.psi.ConfVarAssignment;
import org.andreapeter.antconf.psi.ConfVarValue;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ConfAnnotator implements Annotator
{

    @Override
    public void annotate(@NotNull PsiElement element, @NotNull AnnotationHolder holder)
    {
        if (element instanceof ConfVarAssignment) {
            ConfVarAssignment assignment = (ConfVarAssignment) element;
            ConfVarValue varValue = assignment.getVarValue();
            if (varValue != null) {
                checkVarValue(assignment, varValue, holder);
            }
        }
    }

    /**
     * Checks variable value and annotates if necessary
     * @param assignment The variable assignment element
     * @param varValue The variable value element
     * @param holder The annotation holder
     */
    private void checkVarValue(@NotNull ConfVarAssignment assignment,
                               @NotNull ConfVarValue varValue,
                               @NotNull AnnotationHolder holder) {
        String value = varValue.getText();
        String assignmentType = assignment.getAssignmentType();
        // Check if it is not an append, that the value does not start or end with a space
        if (!assignmentType.equals("Var.Append")) {
            if (value.startsWith(" ")) {
                // Variable value starts with whitespace
                holder.newAnnotation(
                        HighlightSeverity.WARNING,
                        "Variable starts with whitespace")
                        // TODO we could easily add a quick fix here
                        .range(varValue.getTextRange())
                        .highlightType(ProblemHighlightType.WARNING).create();
            } if (value.endsWith(" ")) {
                // Variable value ends with whitespace
                holder.newAnnotation(
                        HighlightSeverity.WARNING,
                        "Variable ends with whitespace")
                        // TODO we could easily add a quick fix here
                        .range(varValue.getTextRange())
                        .highlightType(ProblemHighlightType.WARNING).create();
            }
        }
        // Check if variable content is outside of Latin1, that would generate an error
        Charset charSet = StandardCharsets.ISO_8859_1;
        if (!isStringInCharset(value, charSet))
        {
            holder.newAnnotation(
                    HighlightSeverity.ERROR,
                    String.format("Variable value is not contained in %s character set", charSet))
                    .range(varValue.getTextRange())
                    .highlightType(ProblemHighlightType.ERROR).create();
        }

        // Check if variable content is outside of ASCII charset, that would generate a warning
        charSet = StandardCharsets.US_ASCII;
        if (!isStringInCharset(value, charSet)) {
            holder.newAnnotation(
                    HighlightSeverity.WARNING,
                    String.format("Variable value is not contained in %s character set", charSet))
                    .range(varValue.getTextRange())
                    .highlightType(ProblemHighlightType.WARNING).create();
        }
    }

    /**
     * Tests whether the given string can be represented in the given charset
     * @param s The string to test
     * @param charSet The target charset
     * @return true whether the given string is representable in the given charset
     */
    private boolean isStringInCharset(String s, Charset charSet) {
        String tmp = new String(s.getBytes(charSet), charSet);
        return tmp.equals(s);
    }
}
