package org.andreapeter.antconf;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class ConfFileType extends LanguageFileType {
    public static final ConfFileType INSTANCE = new ConfFileType();

    private ConfFileType() {
        super(ConfLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "oberon_conf";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Oberon configuration";
    }

    @NotNull
    @Override
    // TODO in xo2 it's "Conf"
    public String getDefaultExtension() {
        return "conf";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return Icons.CONF_FILE;
    }
}
