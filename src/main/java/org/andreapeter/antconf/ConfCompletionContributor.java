package org.andreapeter.antconf;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.util.ProcessingContext;
import org.andreapeter.antconf.psi.ConfTypes;
import org.jetbrains.annotations.NotNull;

// Not used, but we keep it as an example in case it becomes useful
// This class contributes to completions other than references
public class ConfCompletionContributor extends CompletionContributor
{
    public ConfCompletionContributor() {
        extend(CompletionType.BASIC, PlatformPatterns.psiElement(ConfTypes.VAR_NAME_DEF),
            new CompletionProvider<CompletionParameters>() {
                public void addCompletions(@NotNull CompletionParameters parameters,
                                           @NotNull ProcessingContext context,
                                           @NotNull CompletionResultSet resultSet) {
                    // TODO implement proper completion
                    resultSet.addElement(LookupElementBuilder.create("Hello"));
                }
            }
        );
    }
}
