package org.andreapeter.antconf;

import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.*;
import org.andreapeter.antconf.psi.ConfVarExpansion;
import org.andreapeter.antconf.psi.ConfVarNameDef;
import org.andreapeter.antconf.psi.ConfVarNameExp;
import org.jetbrains.annotations.NotNull;

public class ConfReferenceContributor extends PsiReferenceContributor
{
    @Override
    public void registerReferenceProviders(@NotNull PsiReferenceRegistrar registrar)
    {
        registrar.registerReferenceProvider(
                PlatformPatterns.psiElement(ConfVarNameExp.class),
                new ConfVarNameExpReferenceProvider());
//        registrar.registerReferenceProvider(
//                PlatformPatterns.psiElement(ConfVarNameExp.class),
//                new ConfVarNameExpReferenceProvider());
    }
}
