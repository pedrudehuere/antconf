package org.andreapeter.antconf;

import com.intellij.lexer.FlexAdapter;
import org.andreapeter.antconf.lexer.ConfLexer;

public class ConfLexerAdapter extends FlexAdapter {

    public ConfLexerAdapter() {
        super(new ConfLexer());
    }
}
