package org.andreapeter.antconf;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementResolveResult;
import com.intellij.psi.PsiPolyVariantReference;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.ResolveResult;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.andreapeter.antconf.psi.ConfVarAssignment;
import org.andreapeter.antconf.psi.ConfVarNameDef;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * Reference of a variable name in a variable expansion
 */
public class ConfVarNameExpReference extends PsiReferenceBase<PsiElement> implements PsiPolyVariantReference
{
    // Create logger with category "ConfLexer"
    private static final Logger log = Logger.getInstance("ConfVarAssignmentReference");
    private final String varName;

    public ConfVarNameExpReference(@NotNull PsiElement element, TextRange textRange) {
        super(element, textRange);
        varName = element.getText().substring(textRange.getStartOffset(), textRange.getEndOffset());
    }

    @Override
    public boolean isReferenceTo(@NotNull PsiElement element) {
        // Basically, if the given element matches any element we resolve we are good
        ResolveResult[] results = multiResolve(false);
        for (ResolveResult result : results) {
            PsiElement resultElement = result.getElement();
            if (resultElement != null) {
                if (resultElement instanceof ConfVarAssignment) {
                    ConfVarNameDef varNameDef = ((ConfVarAssignment) resultElement).getVarNameDef();
                    if (varNameDef != null) {
                        if (element.getManager().areElementsEquivalent(varNameDef, element)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
  }

    @NotNull
    @Override
    public ResolveResult @NotNull [] multiResolve(boolean incompleteCode) {
        Project project = myElement.getProject();
        final List<ConfVarAssignment> assignments =
                ConfUtil.findConfVarAssignmentsByVarName(project, varName);
        List<ResolveResult> results = new ArrayList<>();
        for (ConfVarAssignment assignment: assignments) {
            results.add(new PsiElementResolveResult(assignment));
        }
        return results.toArray(new ResolveResult[results.size()]);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        ResolveResult[] resolveResults = multiResolve(false);
        return resolveResults.length == 1 ? resolveResults[0].getElement() : null;
    }

    /**
     * For now, we just return a list of unique strings, if one day we implement
     * parsing of .aMake files we will return each occurrence with the filename and the line number
     */
    @Override
    public Object @NotNull[] getVariants() {
        Project project = myElement.getProject();
         List<ConfVarAssignment> assignments = ConfUtil.findAllConfVarAssignments(project);
        Set<LookupElement> variants = new HashSet<>();
        for (final ConfVarAssignment assignment: assignments) {
            if (assignment.getVariableName() != null && !assignment.getVariableName().isEmpty()) {
                // The decoration of completions can be decorated here
                variants.add(LookupElementBuilder.
                        create(assignment.getVariableName()).
                        withIcon(Icons.CONF_FILE));
            }
        }
        return variants.toArray();
    }
}
