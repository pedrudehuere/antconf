package org.andreapeter.antconf;

import com.intellij.lang.Language;

public class ConfLanguage extends Language {
    public static final ConfLanguage INSTANCE = new ConfLanguage();

    private ConfLanguage() {
        super("ant_conf");
    }
}
