package org.andreapeter.antconf;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import org.andreapeter.antconf.psi.ConfVarExpansion;
import org.andreapeter.antconf.psi.ConfVarNameExp;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;


/**
 * Reference of a variable name in a variable definition (assignment)
 */
public class ConfVarNameDefReference extends PsiReferenceBase<PsiElement> implements PsiPolyVariantReference
{
    private final String varName;

    public ConfVarNameDefReference(@NotNull PsiElement element, TextRange textRange) {
        super(element, textRange);
        varName = element.getText().substring(textRange.getStartOffset(), textRange.getEndOffset());
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incompleteCode) {
        Project project = myElement.getProject();
        final List<ConfVarExpansion> varExpansions = ConfUtil.findConfVarExpansions(project, varName);
        List<ResolveResult> results = new ArrayList<>();
        for (ConfVarExpansion varExpansion: varExpansions) {
            ConfVarNameExp varName = varExpansion.getVarNameExp();
            if (varName != null) {
                results.add(new PsiElementResolveResult(varName));
            }
        }
        return results.toArray(new ResolveResult[results.size()]);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        ResolveResult[] resolveResults = multiResolve(false);
        return resolveResults.length == 1 ? resolveResults[0].getElement() : null;
    }

    /**
     * For now, we just return a list of unique strings, if one day we implement
     * parsing of .aMake files we will return each occurrence with the filename and the line number
     */
    @Override
    public Object @NotNull[] getVariants() {
        Project project = myElement.getProject();
        List<ConfVarExpansion> varExpansions = ConfUtil.findConfVarExpansions(project);
        Set<LookupElement> variants = new HashSet<>();
        for (final ConfVarExpansion varExpansion: varExpansions) {
            if (varExpansion.getVariableName() != null && varExpansion.getVariableName().length() > 0) {
                // The decoration of completions can be decorated here
                variants.add(LookupElementBuilder.
                        create(varExpansion.getVariableName()).
                        withIcon(Icons.CONF_FILE));
            }
        }
        return variants.toArray();
    }
}
