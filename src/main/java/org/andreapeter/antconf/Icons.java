package org.andreapeter.antconf;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class Icons
{
	public static final Icon CONF_FILE = IconLoader.getIcon("/icons/pirate.png");
}
