package org.andreapeter.antconf;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

/**
 * Provides references for the VARNAME element
 */
public class ConfVarNameDefReferenceProvider extends PsiReferenceProvider
{
    @Override
    public PsiReference @NotNull [] getReferencesByElement(@NotNull PsiElement element,
                                                           @NotNull ProcessingContext context)
    {
        return new PsiReference[]{
            new ConfVarNameDefReference(element, new TextRange(0, element.getTextLength()))
        };
    }
}
