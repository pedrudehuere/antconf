package org.andreapeter.antconf.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.andreapeter.antconf.ConfFileType;
import org.andreapeter.antconf.ConfLanguage;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ListIterator;

public class ConfFile extends PsiFileBase {

    // When we try to find the Oberon folder,
    // we give up after this amount of parent directories
    private final int maxOberonDirRecursion = 10;
    private final String myOberonPath;


    public ConfFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, ConfLanguage.INSTANCE);
        myOberonPath = computeOberonPath();
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return ConfFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Conf File";
    }

    public String getOberonPath() {
        return myOberonPath;
    }

    /**
     * Gets path relative to the Oberon folder,
     * if it fails, returns filename
     */
    private String computeOberonPath() {

        if (getContainingDirectory() == null
                || getContainingDirectory().getVirtualFile().getCanonicalPath() == null) {
            return getName();
        }

        Path parent = Paths.get(getContainingDirectory().getVirtualFile().getCanonicalPath());
        int attempts = 0;
        ArrayList<String> elements = new ArrayList<>();

        while (parent != null
                && parent.getNameCount() > 0
                && !parent.endsWith("Oberon")
                && attempts <= maxOberonDirRecursion) {

            elements.add(parent.getName(parent.getNameCount() - 1).toString());
            parent = parent.getParent();
            attempts++;
        }
        if (attempts >= maxOberonDirRecursion || parent == null) {
            return getName();
        }

        StringBuilder retPath = new StringBuilder();
        ListIterator<String> iterator = elements.listIterator(elements.size());

        while(iterator.hasPrevious()) {
            retPath.append(iterator.previous()).append("/");
        }

        return retPath + getName();
    }
}
