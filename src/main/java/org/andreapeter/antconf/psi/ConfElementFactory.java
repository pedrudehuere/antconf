package org.andreapeter.antconf.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFileFactory;
import org.andreapeter.antconf.ConfFileType;

public class ConfElementFactory
{
    /**
     *
     * @param project Our project
     * @param variableName The variable name in "Var.Set VARNAME"
     * @return A new variable assignment
     */
    public static ConfVarAssignment createVarAssignment(Project project, String variableName) {
        final ConfFile file = createVarAssignmentFile(project, variableName);
        return (ConfVarAssignment) file.getFirstChild();
    }

    /**
     * Create a file with a single variable assignemnt
     * @param project Our project
     * @param varname The variable name in "Var.Set VARNAME"
     * @return A new file
     */
    private static ConfFile createVarAssignmentFile(Project project, String varname)
    {
        String name = "dummy.conf";

        String fileContent = "Var.Set ";
        fileContent += varname;

        return (ConfFile) PsiFileFactory.getInstance(project).
                createFileFromText(name, ConfFileType.INSTANCE, fileContent);
    }
}
