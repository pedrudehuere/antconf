package org.andreapeter.antconf.psi;

import com.intellij.psi.tree.IElementType;
import org.andreapeter.antconf.ConfLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class ConfElementType extends IElementType {
  public ConfElementType(@NotNull @NonNls String debugName) {
    super(debugName, ConfLanguage.INSTANCE);
  }
}
