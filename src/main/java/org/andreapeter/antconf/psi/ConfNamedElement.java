package org.andreapeter.antconf.psi;

import com.intellij.psi.PsiNameIdentifierOwner;

public interface ConfNamedElement extends PsiNameIdentifierOwner
{
}
