// Copyright 2000-2022 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license.

package org.andreapeter.antconf.psi;

import com.intellij.psi.tree.TokenSet;


public interface ConfTokenSets {
  TokenSet COMMENTS = TokenSet.create();
  TokenSet IDENTIFIERS = TokenSet.create(ConfTypes.VARNAME, ConfTypes.VAR_NAME_EXP);
  TokenSet LITERALS = TokenSet.create(ConfTypes.SOME_TEXT);
}
