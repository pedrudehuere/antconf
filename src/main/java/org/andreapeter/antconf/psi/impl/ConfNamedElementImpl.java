package org.andreapeter.antconf.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.navigation.NavigationItem;
import com.intellij.psi.PsiFile;
import org.andreapeter.antconf.Icons;
import org.andreapeter.antconf.psi.ConfFile;
import org.andreapeter.antconf.psi.ConfNamedElement;
import org.jetbrains.annotations.NotNull;

import javax.swing.Icon;

public abstract class ConfNamedElementImpl extends ASTWrapperPsiElement implements ConfNamedElement, NavigationItem
{
    public ConfNamedElementImpl(@NotNull ASTNode node) {
        super(node);
    }

    @Override
    public ItemPresentation getPresentation()
    {
        return new ConfItemPresentation(getPresentationText(), getPresentationLocation());
    }

    public Icon getIcon(@IconFlags int flags){
        return Icons.CONF_FILE;
    }

    public String getPresentationText()
    {
        return getName();
    }

    private String getPresentationLocation() {
        String location = null;
        PsiFile file = getContainingFile();

        if (file instanceof ConfFile) {
            location = ((ConfFile) file).getOberonPath();
            if (location == null) {
                location = "<unknown project>" + file.getName();
            }
        } else {
            // We should never arrive here
            location = "<unknown project>" + file.getName();
        }
        return location;
    }
}
