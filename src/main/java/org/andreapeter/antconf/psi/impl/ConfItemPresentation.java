package org.andreapeter.antconf.psi.impl;

import com.intellij.navigation.ItemPresentation;
import com.intellij.openapi.util.NlsSafe;
import org.andreapeter.antconf.Icons;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class ConfItemPresentation implements ItemPresentation
{

    String myText;
    String myLocation;

    ConfItemPresentation(String text, String location) {
        myText = text;
        myLocation = location;
    }

    @Override
    public @NlsSafe @Nullable String getPresentableText()
    {
        return myText;
    }

    @Override
    public @NlsSafe @Nullable String getLocationString()
    {
        return myLocation;
    }

    @Override
    public @Nullable Icon getIcon(boolean unused)
    {
        return Icons.CONF_FILE;
    }
}
