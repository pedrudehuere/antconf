package org.andreapeter.antconf.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.impl.source.resolve.reference.ReferenceProvidersRegistry;
import com.intellij.psi.util.PsiTreeUtil;
import org.andreapeter.antconf.psi.*;
import org.apache.commons.lang.StringUtils;


public class ConfPsiImplUtil
{
    public static String getAssignmentType(ConfVarAssignment element) {
        PsiElement varAssigner = PsiTreeUtil.getChildOfType(element, ConfVarAssigner.class);
        if (varAssigner == null) {
            return "<unknown>";
        }
        return varAssigner.getText();
    }

    public static String getPresentationText(ConfVarAssignment element) {

        int maxLen = 20;

        // Get variable value
        String val = null;
        ConfVarValue varValue = element.getVarValue();
        if (varValue != null) {
            val = varValue.getText();
        }

        // variables can be declared with no value
        if (val != null) {
            if (val.length() + 2 > maxLen) {
                val = '"' + val.substring(0, maxLen - 5) + "...\"";
            } else {
                val = StringUtils.rightPad( '"' + val + '"', maxLen);
            }
            return StringUtils.rightPad(element.getAssignmentType(), 10) + " " + val;
        }
        else {
            return StringUtils.rightPad(element.getAssignmentType(), 10) + " " + StringUtils.rightPad("\"\"", maxLen);
        }
    }

    public static int getTextOffset(ConfVarAssignment element) {
        PsiElement nameIdentifier = element.getNameIdentifier();
        if (nameIdentifier != null) {
            return nameIdentifier.getTextOffset();
        }
        return 0;
    }

    private static String getVariableName(ConfVarNameDef element) {
        ASTNode varnameNode = element.getNode();
        if (varnameNode != null) {
            // IMPORTANT: Convert embedded escaped spaces to simple spaces
            // TODO not sure this is important, copied from tutorial
            return varnameNode.getText().replaceAll("\\\\ ", " ");
        } else {
            return null;
        }
    }

    public static String getVariableName(ConfVarAssignment element) {
        ASTNode varnameNode = element.getNode().findChildByType(ConfTypes.VAR_NAME_DEF);
        if (varnameNode != null) {
            // IMPORTANT: Convert embedded escaped spaces to simple spaces
            // TODO not sure this is important, copied from tutorial
            return varnameNode.getText().replaceAll("\\\\ ", " ");
        } else {
            return null;
        }
    }

    public static String getVariableName(ConfVarExpansion element) {
        ASTNode varNameNode = element.getNode().findChildByType(ConfTypes.VAR_NAME_EXP);
        if (varNameNode != null) {
            return varNameNode.getText().replaceAll("\\\\", " ");
        } else {
            return null;
        }
    }

    public static String getName(ConfVarAssignment element) {
        return getVariableName(element);
    }

    public static String getName(ConfVarNameDef element) {
        return getVariableName(element);
    }

    public static PsiElement setName(ConfVarAssignment element, String newName) {
        ASTNode varnameNode = element.getNode().findChildByType(ConfTypes.VAR_NAME_DEF);
        if (varnameNode != null)
        {
            ConfVarAssignment assignment =
                    ConfElementFactory.createVarAssignment(element.getProject(), newName);
            ASTNode newVarnameNode = assignment.getNode().findChildByType(ConfTypes.VAR_NAME_DEF);
            element.getNode().replaceChild(varnameNode, newVarnameNode);
        }
        return element;
    }

    public static PsiElement setName(ConfVarNameDef element, String newName) {
        ASTNode varnameNode = element.getNode();
        if (varnameNode != null)
        {
            ConfVarAssignment assignment =
                    ConfElementFactory.createVarAssignment(element.getProject(), newName);
            ASTNode newVarnameNode = assignment.getNode().findChildByType(ConfTypes.VAR_NAME_DEF);
            element.getNode().replaceChild(varnameNode, newVarnameNode);
        }
        return element;
    }

    public static PsiElement getNameIdentifier(ConfVarAssignment element) {
        ASTNode varnameNode = element.getNode().findChildByType(ConfTypes.VAR_NAME_DEF);
        if (varnameNode != null)
        {
            return varnameNode.getPsi();
        } else {
            return null;
        }
    }

    public static PsiElement getNameIdentifier(ConfVarNameDef element) {
        ASTNode varnameNode = element.getNode();
        if (varnameNode != null)
        {
            return varnameNode.getPsi();
        } else {
            return null;
        }
    }

    public static PsiReference[] getReferences(ConfVarNameDef element) {
        return ReferenceProvidersRegistry.getReferencesFromProviders(element);
    }

    public static PsiReference[] getReferences(ConfVarNameExp element) {
        return ReferenceProvidersRegistry.getReferencesFromProviders(element);
    }
}
