package org.andreapeter.antconf.psi;

import com.intellij.openapi.components.Service;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.intellij.psi.util.CachedValue;
import com.intellij.psi.util.CachedValueProvider;
import com.intellij.psi.util.CachedValuesManager;
import com.intellij.psi.util.PsiModificationTracker;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import org.jetbrains.annotations.NotNull;


/**
 * Caches Conf PSI elements, for now:
 *  - All variable assignments
 *  - Variable assignments by variable name
 */
@Service(Service.Level.PROJECT)
public final class ConfPsiCache {

    private final Project project;

    private final String keyBase = "org.andreapeter.antconf.";
    private final Key<CachedValue<List<ConfVarAssignment>>> allVarAssignmentsKey =
            new Key<>(keyBase + "AllConfVarAssignments");
    private final Key<CachedValue<Map<String, List<ConfVarAssignment>>>> varAssignmentByNameKey =
            new Key<>(keyBase + "ConfVarAssignmentsByName");


    public ConfPsiCache() {
        // Let's hope this is never called
        this.project = null;
    }

    public ConfPsiCache(Project project) {
        this.project = project;
    }

    /**
     * Find all variable assignments in project, return value is cached,
     * cache is invalidated on any physical PSI change
     * @param supplier Gets all variable assignment, called only if cached value is invalid
     * @return All variable assignments
     */
    public @NotNull List<ConfVarAssignment> findAllConfVarAssignments(Supplier<List<ConfVarAssignment>> supplier) {
        return CachedValuesManager.getManager(project).getCachedValue(
                project,
                allVarAssignmentsKey,
                () -> CachedValueProvider.Result.create(supplier.get(),
                    PsiModificationTracker.MODIFICATION_COUNT),
                false
                );
    }

    /**
     * Find variable assignments with given variable name
     * @param name Variable name for which we search variable assignments
     * @param supplier Gets all variable assignments with given name, only called if cache is invalid
     * @return Variable assignments that assign variable with given name
     */
    public @NotNull List<ConfVarAssignment> findConfVarAssignmentsByName(
            String name, Supplier<Map<String, List<ConfVarAssignment>>> supplier) {
        Map<String, List<ConfVarAssignment>> varAssignmentsByName =
                CachedValuesManager.getManager(project).getCachedValue(
                        project,
                        varAssignmentByNameKey,
                        () -> CachedValueProvider.Result.create(supplier.get(),
                                PsiModificationTracker.MODIFICATION_COUNT),
                        false);
        return varAssignmentsByName.getOrDefault(name, Collections.emptyList());
    }
}
