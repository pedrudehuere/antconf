package org.andreapeter.antconf.psi;

import com.intellij.psi.tree.IElementType;
import org.andreapeter.antconf.ConfLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class ConfTokenType extends IElementType {
  public ConfTokenType(@NotNull @NonNls String debugName) {
    super(debugName, ConfLanguage.INSTANCE);
  }

  @Override
  public String toString() {
    return /* "ConfTokenType." + */ super.toString();
  }
}
