ANT conf
========

ANT configuration files support for IntelliJ and friends

# Roadmap
## Commands
 - [x] Var.Set and Var.SetD
 - [x] Script.Print
 - [x] Var.Append
 - [x] Arbitrary commands
 - [x] Flow control (if else)
 - [x] Var.SetE and Var.SetF

## Expansions
 - [x] Simple variable expansion
 - [x] Nested simple variable expansion
 - [x] Simple variable expansion with default value
 - [x] Switch case variable expansion
 - [x] Reverse Polish notation variable expansion
 - [x] Command expansion
 - [ ] Expansions in variable names
 - [ ] Expansions in variable values
   
## Syntax highlighting
 - [x] Syntax highlighting vor variable expansions
 - [x] Syntax highlighting for commands
 - [ ] Mark text in Script.Print and similar commands
 - [x] Darcula color scheme
 - [ ] Intellij-light color scheme

## Autocompletion and references
 - [x] Autocomplete variable names in variable expansions
 - [x] Autocomplete variable names in incomplete variable expansions
   (while typing)
 - [ ] Autocomplete variable names in var set commands while typing
 - [x] Go to definition (CTRL-click) of variable in variable expansions
 - [ ] Find usages of variables from a var assignment

## Project structure
 - [ ] Automatic search scope in current project
 - [ ] Create .conf file

# Maybe for the future
 - [ ] Support .xconf files
 - [x] Improve lexer debugging
